//
//  BaseVC.swift
//  NoBolo
//
//  Created by Usman on 4/25/19.
//  Copyright © 2019 Usman. All rights reserved.
//



import UIKit
import SDWebImage
class BaseVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setLeftMenuButton()
        removeBackButtonText()
    }
    
    func isValidEmail(testStr:String) -> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        print(testStr)
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func setNavBarTitle(title: String) {
        
        let navLabel = UILabel()
        navLabel.text  = title
        navLabel.font = UIFont.systemFont(ofSize: 17)
        navLabel.textColor = UIColor.white
        self.navigationItem.titleView = navLabel
    }
    func hideRequestView() {
        
        
    }
}
public enum ControllerType{
    case Offer,Deal,EditRecentWork,AddRecentWork,BrandPackage,BrandOffer,Package
}

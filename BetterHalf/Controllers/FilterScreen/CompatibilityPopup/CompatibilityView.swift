//
//  CompatibilityView.swift
//  BetterHalf
//
//  Created by Usman on 13/02/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

class CompatibilityView: UIView {
    //MARK: - IBOUTLETS
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var startBtn: UIButton!
    var continueBtnTapped:((Int)->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        let view = instanceFromNib()
        view.frame = bounds
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        // Show the view.
        addSubview(view)
        
    }
    
    private func instanceFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func mayBeLaterTapped(_ sender: UIButton){
        self.removeFromSuperview()
    }
    
    @IBAction func continueBtnTapped(_ sender: UIButton){
        self.continueBtnTapped?(sender.tag)
    }

}
class CompatibilityGradientButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    public lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [UIColor(hexString: "#ff62a5").cgColor, UIColor(hexString: "#ff8960").cgColor]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.cornerRadius = 25
        layer.insertSublayer(l, at: 0)
        return l
    }()
}


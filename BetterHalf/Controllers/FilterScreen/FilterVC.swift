//
//  SignInVC.swift
//  NoBolo
//
//  Created by Usman on 9/25/19.
//  Copyright © 2019 Mudassar. All rights reserved.
//

import UIKit


class FilterVC: BaseVC {
    //MARK: - IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    var model: FilterModel?
    var dataArr = [Constants.COMPATIBILITY,Constants.CATEGORY,Constants.LOCATION,Constants.DISTANCE,Constants.AGE,Constants.WEIGHT,Constants.HEIGHT,Constants.EDUCATION,Constants.MARRIAGE_HISTORY]
    var testArr = [TestModel]()
    //MARK:- FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetup()
        let righBtn = UIBarButtonItem(image: UIImage(named: "wh_logo"), style: .plain, target: self, action: #selector(rightBarBtnTapped(_:)))
        self.navigationItem.rightBarButtonItem = righBtn
    }
    
    @objc func rightBarBtnTapped(_ sender: UIBarButtonItem){
        var params = Dictionary<String,Any>()
        params["distance"] = (self.model?.distanceSlider?.selectedValue ?? 0.0) as Any
        params["min_age"] = (self.model?.ageSlider?.selectedValue ?? 0.0) as Any
        params["max_age"] = (self.model?.ageSlider?.selectedMaxValue ?? 0.0) as Any
        params["min_weight"] = (self.model?.weightSlider?.selectedValue ?? 0.0) as Any
        params["max_weight"] = (self.model?.weightSlider?.selectedMaxValue ?? 0.0) as Any
        params["min_height"] = (self.model?.heightSlider?.selectedValue ?? 0.0) as Any
        params["max_height"] = (self.model?.heightSlider?.selectedMaxValue ?? 0.0) as Any
        params["categories"] = self.model?.categoryArr?.compactMap({$0.isSelected ?? false ? $0.title:nil})
        print(params)
    }
    
    func tableViewSetup(){
        self.setupData()
        tableView.register(UINib(nibName: ButtonCell.className, bundle: nil), forCellReuseIdentifier: ButtonCell.className)
        tableView.register(UINib(nibName: CategoriesCell.className, bundle: nil), forCellReuseIdentifier: CategoriesCell.className)
        tableView.register(UINib(nibName: SliderCell.className, bundle: nil), forCellReuseIdentifier: SliderCell.className)
    }
    
    func setupData(){
        let arr = ["Religious","Family","Politics","Outgoing","Forgiving","Finances","Charity","Kindness","Orderly","Social","Introvert","Law Abiding","Cooperative","Multitasking"]
        let eduArr = ["High School","Bachelor","Master","PhD"]
        let marrArr = ["Never","Once","Twice","More than twice",]
        var categoryArr = [CategoryButton]()
        var educationArr = [CategoryButton]()
        var marriageArr = [CategoryButton]()
        let distance = SliderModel.init(heading: Constants.DISTANCE, value: "100 miles", type: .SingleThumb, min: 0, max: 150, selected: 100, selectedMax: nil)
        let age = SliderModel.init(heading: Constants.AGE, value: "18-30", type: .MultiThumb, min: 17, max: 100, selected: 18, selectedMax: 30)
        let weight = SliderModel.init(heading: Constants.WEIGHT, value: "50-100 kg", type: .MultiThumb, min: 0, max: 200, selected: 50, selectedMax: 100)
        let height = SliderModel.init(heading: Constants.HEIGHT, value: "5.1-5.6", type: .MultiThumb, min: 3, max: 7, selected: 5.1, selectedMax: 5.6)
        for obj in arr{
            let obj = CategoryButton(title: obj, isSelected: false)
            categoryArr.append(obj)
        }
        for obj in eduArr{
            let obj = CategoryButton(title: obj, isSelected: false)
            educationArr.append(obj)
        }
        for obj in marrArr{
            let obj = CategoryButton(title: obj, isSelected: false)
            marriageArr.append(obj)
        }
        self.model = FilterModel(catArr: categoryArr, eduArr: educationArr, marrArr: marriageArr, distance: distance, age: age, weight: weight, heigt: height)
        //MODEL FOR TEST
        var dict = Dictionary<String,Any>()
        dict["title"] = "I love order in my life." as Any
        dict["footerHeading"] = "Psychology Fun Fact" as Any
        dict["footerDetail"] = "The way you organize your email inbox may reveal how controlling you are." as Any
        dict["footerImage"] = "group"
        dict["buttonSize"] = "small"
        let buttonArr:[String] = ["Yes","Sometimes","No"]
        dict["buttonArr"] = buttonArr as Any
        //MODEL 2
        var dict2 = Dictionary<String,Any>()
        dict2["title"] = "I trust that things will work out." as Any
        dict2["footerHeading"] = "" as Any
        dict2["footerDetail"] = "" as Any
        dict2["bottomImage"] = "artwork"
        dict2["buttonSize"] = "small"
        let buttonArr2:[String] = ["Yes","Usually","No"]
        dict2["buttonArr"] = buttonArr2 as Any
        //MODEL 3
        var dict3 = Dictionary<String,Any>()
        dict3["title"] = "I take time to help others." as Any
        dict3["footerHeading"] = "" as Any
        dict3["footerDetail"] = "" as Any
        dict3["footerImage"] = ""
        dict3["buttonSize"] = "large"
        let buttonArr3:[String] = ["Yes","Usually","Circumstances Permitting","Rarely","No"]
        dict3["buttonArr"] = buttonArr3 as Any
        //MODEL 4
        var dict4 = Dictionary<String,Any>()
        dict4["title"] = "I am talkative with others." as Any
        dict4["footerHeading"] = "Psychology Fun Fact" as Any
        dict4["footerDetail"] = "Extroverts are more likely to engage in risk-taking behaviors." as Any
        dict4["footerImage"] = "group"
        dict4["buttonSize"] = "medium"
        let buttonArr4:[String] = ["Yes","Sometimes","Rarely","No"]
        dict4["buttonArr"] = buttonArr4 as Any
        //MODEL 5
        var dict5 = Dictionary<String,Any>()
        dict5["title"] = "I want to help those less fortunate than me." as Any
        dict5["footerHeading"] = "" as Any
        dict5["footerDetail"] = "" as Any
        dict5["bottomImage"] = "helpArtwork"
        dict5["buttonSize"] = "medium"
        let buttonArr5:[String] = ["Yes","As able","Occasionally","No"]
        dict5["buttonArr"] = buttonArr5 as Any
        //MODEL 6
        var dict6 = Dictionary<String,Any>()
        dict6["title"] = "I’m expressive about my emotions." as Any
        dict6["footerHeading"] = "" as Any
        dict6["footerDetail"] = "" as Any
        dict6["footerImage"] = ""
        dict6["buttonSize"] = "medium"
        let buttonArr6:[String] = ["Yes","Usually","Sometimes","Rarely","No"]
        dict6["buttonArr"] = buttonArr6 as Any
        //MODEL 7
        var dict7 = Dictionary<String,Any>()
        dict7["title"] = "I find being around people energizing and rewarding." as Any
        dict7["footerHeading"] = "" as Any
        dict7["footerDetail"] = "" as Any
        dict7["bottomImage"] = "group87"
        dict7["buttonSize"] = "small"
        let buttonArr7:[String] = ["Yes","Mostly","No"]
        dict7["buttonArr"] = buttonArr7 as Any
        //MODEL 8
        var dict8 = Dictionary<String,Any>()
        dict8["title"] = "I enjoy my own company." as Any
        dict8["footerHeading"] = "" as Any
        dict8["footerDetail"] = "" as Any
        dict8["bottomImage"] = "ownArtwork"
        dict8["buttonSize"] = "small"
        let buttonArr8:[String] = ["Yes","No"]
        dict8["buttonArr"] = buttonArr8 as Any
        //MODEL 9
        var dict9 = Dictionary<String,Any>()
        dict9["title"] = "I get overwhelmed easily (too many tasks, too little help)." as Any
        dict9["footerHeading"] = "Psychology Fun Fact" as Any
        dict9["footerDetail"] = "People get overwhelmed easily are likely to be Highly Sentitive  Person(HSP)." as Any
        dict9["footerImage"] = "group"
        dict9["buttonSize"] = "medium"
        let buttonArr9:[String] = ["Yes","Sometimes","No"]
        dict9["buttonArr"] = buttonArr9 as Any
        //MODEL 10
        var dict10 = Dictionary<String,Any>()
        dict10["title"] = "I am looking for a partner with a personality" as Any
        dict10["footerHeading"] = "" as Any
        dict10["footerDetail"] = "" as Any
        dict10["footerImage"] = ""
        dict10["buttonSize"] = "large"
        let buttonArr10:[String] = ["Similar to myself","Opposite to myself","Somewhat like me","Doesn’t matter","Other","Sometimes","No"]
        dict10["buttonArr"] = buttonArr10 as Any
        //MODEL 11
        var dict11 = Dictionary<String,Any>()
        dict11["title"] = "My friend would say I am angry." as Any
        dict11["footerHeading"] = "" as Any
        dict11["footerDetail"] = "" as Any
        dict11["bottomImage"] = "group158"
        dict11["buttonSize"] = "small"
        let buttonArr11:[String] = ["Yes","No"]
        dict11["buttonArr"] = buttonArr11 as Any
        let dictArr = [dict,dict2,dict3,dict4,dict5,dict6,dict7,dict8,dict9,dict10,dict11]
        for obj in dictArr{
            let model = TestModel.init(dictionary: obj)
            self.testArr.append(model)
        }
        
    }
    
    

}
extension FilterVC: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = dataArr[indexPath.row]
        switch obj {
        case Constants.COMPATIBILITY:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.className, for: indexPath) as? ButtonCell{
                cell.configCell(obj: ButtonCellModel(headingText: "Compatibility", titleText: "Compatibility Tests", resultText: "0%",showLefticon: true,cellType: Constants.COMPATIBILITY))
                return cell
            }
            case Constants.LOCATION:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.className, for: indexPath) as? ButtonCell{
                cell.configCell(obj: ButtonCellModel(headingText: Constants.LOCATION, titleText: "Current location", resultText: "(San Francisco)",showLefticon: false,cellType: Constants.LOCATION))
                return cell
            }
        case Constants.CATEGORY:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesCell.className, for: indexPath) as? CategoriesCell{
                cell.cellType = .Category
                cell.model = self.model
                cell.setupCollection()
                return cell
            }
        case Constants.DISTANCE:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SliderCell.className, for: indexPath) as? SliderCell{
                cell.cellConfig(obj: self.model?.distanceSlider)
                cell.sliderValueChanged = {value,_ in
                    self.model?.distanceSlider?.selectedValue = value
                
                }
                return cell
            }
            case Constants.AGE:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SliderCell.className, for: indexPath) as? SliderCell{
                cell.cellConfig(obj: self.model?.ageSlider)
                cell.sliderValueChanged = {minSelectval,maxSelectVal in
                    self.model?.ageSlider?.selectedValue = minSelectval
                    self.model?.ageSlider?.selectedMaxValue = maxSelectVal
                    cell.valueLbl.text = "\(String(format:"%.0f", minSelectval ?? 0.0)) - \(String(format:"%.0f", maxSelectVal ?? 0.0))"
                }
                return cell
            }
            case Constants.WEIGHT:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SliderCell.className, for: indexPath) as? SliderCell{
                cell.cellConfig(obj: self.model?.weightSlider)
                cell.sliderValueChanged = {minSelectval,maxSelectVal in
                    self.model?.weightSlider?.selectedValue = minSelectval
                    self.model?.weightSlider?.selectedMaxValue = maxSelectVal
                    cell.valueLbl.text = "\(String(format:"%.0f", minSelectval ?? 0.0)) - \(String(format:"%.0f", maxSelectVal ?? 0.0)) kg"
                }
                return cell
            }
            case Constants.HEIGHT:
            if let cell = tableView.dequeueReusableCell(withIdentifier: SliderCell.className, for: indexPath) as? SliderCell{
                cell.cellConfig(obj: self.model?.heightSlider)
                cell.sliderValueChanged = {minSelectval,maxSelectVal in
                    self.model?.heightSlider?.selectedValue = minSelectval
                    self.model?.heightSlider?.selectedMaxValue = maxSelectVal
                    cell.valueLbl.text = "\(String(format:"%.1f", minSelectval ?? 0.0)) - \(String(format:"%.1f", maxSelectVal ?? 0.0))"
                }
                return cell
            }
            case Constants.EDUCATION:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesCell.className, for: indexPath) as? CategoriesCell{
                cell.cellType = .Education
                cell.setupCollection()
                return cell
            }
            case Constants.MARRIAGE_HISTORY:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesCell.className, for: indexPath) as? CategoriesCell{
                cell.cellType = .Marriage
                cell.model = self.model
                cell.setupCollection()
                return cell
            }
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let popup = CompatibilityView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            popup.continueBtnTapped = { tag in
                if tag == 0{
                    popup.detailLbl.text = "Compatibility test improves your search results."
                    popup.startBtn.setTitle("START THE TEST", for: .normal)
                    popup.startBtn.tag = 1
                }else if tag == 1{
                    popup.removeFromSuperview()
                    let testPopup = TestMainView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
                    testPopup.testArr = self.testArr
                    self.view.addSubview(testPopup)
                }
            }
            self.view.addSubview(popup)
        }
    }
}


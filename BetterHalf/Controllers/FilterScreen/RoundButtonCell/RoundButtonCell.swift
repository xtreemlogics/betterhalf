//
//  RoundButtonCell.swift
//  BetterHalf
//
//  Created by Usman on 26/01/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

class RoundButtonCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var textLbl: UILabel!
    var model:FilterModel?
    var btnTapped:(()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(model:CategoryButton?,cellType:CellType?){
        textLbl.text = model?.title
        if cellType == .Category{
            self.backView.layer.cornerRadius = self.backView.frame.height / 2
        }else{
            self.backView.layer.cornerRadius = 5
        }
        
        if cellType == CellType.Education {
            btnIsSelected(bool: self.model?.defaultEducation == model?.title ? true:false)
        }else if cellType == CellType.Marriage{
            btnIsSelected(bool: self.model?.defaultMarriage == model?.title ? true:false)
        }else{
            btnIsSelected(bool: model?.isSelected)
        }
    }
    
    @IBAction func didBtnTapped(_ sender:UIButton){
        self.btnTapped?()
    }
    
    func btnIsSelected(bool:Bool?){
        if (bool ?? false){
            backView.backgroundColor = UIColor(hexString: "#FF689A")
            textLbl.textColor = UIColor.white
            backView.borderColor = UIColor.clear
            backView.borderWidth = 0
        }else{
            backView.backgroundColor = UIColor.white
            textLbl.textColor = UIColor.black
            backView.borderColor = UIColor(hexString: "#DAD9E2")
            backView.borderWidth = 1
        }
    }

}
public enum CellType{
    case Category,Education,Marriage
}

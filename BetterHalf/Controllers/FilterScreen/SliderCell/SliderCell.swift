//
//  SliderCell.swift
//  BetterHalf
//
//  Created by Usman on 26/01/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit
import RangeSeekSlider

class SliderCell: UITableViewCell {
    //MARK: -  IBOUTLETS
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var oneThumbSlider: UISlider!
    @IBOutlet weak var rangeSliderView: RangeSeekSlider!
    var sliderValueChanged:((Float?,Float?)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func cellConfig(obj: SliderModel?){
        headingLbl.text = obj?.heading
        
        rangeSliderView.lineHeight = 5
        oneThumbSlider.setThumbImage(UIImage(named: "slider_dot"), for: .normal)
        if obj?.sliderType == .SingleThumb{
            valueLbl.text = "\(Int(obj?.selectedValue ?? 0.0)) miles"
            oneThumbSlider.isHidden = false
            rangeSliderView.isHidden = true
            oneThumbSlider.minimumValue = obj?.minValue ?? 0.0
            oneThumbSlider.maximumValue = obj?.maxValue ?? 0.0
            oneThumbSlider.value = obj?.selectedValue ?? 0.0
        }else{
            valueLbl.text = "\(Int(obj?.selectedValue ?? 0.0))-\(Int(obj?.selectedMaxValue ?? 0.0))"
            rangeSliderView.delegate = self
            oneThumbSlider.isHidden = true
            rangeSliderView.isHidden = false
            rangeSliderView.minValue = CGFloat(obj?.minValue ?? 0.0)
            rangeSliderView.maxValue = CGFloat(obj?.maxValue ?? 0.0)
            rangeSliderView.selectedMinValue = CGFloat(obj?.selectedValue ?? 0.0)
            rangeSliderView.selectedMaxValue = CGFloat(obj?.selectedMaxValue ?? 0.0)
        }
    }
    
    @IBAction func sliderChanged(_ sender: UISlider){
        valueLbl.text = "\(Int(sender.value)) miles"
        sliderValueChanged?(sender.value,nil)
    }
    
    
}
extension SliderCell: RangeSeekSliderDelegate{
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat){
        valueLbl.text = "\(String(format:"%.1f", Double(minValue))) - \(String(format:"%.1f", Double(maxValue)))"
        sliderValueChanged?(Float(minValue),Float(maxValue))
    }
}


class CustomSlider: UISlider {
  @IBInspectable var trackHeight: CGFloat = 5

  override func trackRect(forBounds bounds: CGRect) -> CGRect {
    return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
  }
}


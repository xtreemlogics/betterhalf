//
//  ButtonCell.swift
//  BetterHalf
//
//  Created by Usman on 26/01/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var leftIconWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK: - CONFIGURE CELL
    func configCell(obj: ButtonCellModel?){
        headingLbl.text = obj?.headingText
        titleLbl.text = obj?.titleText
        textLbl.text = obj?.resultText
        leftIconWidth.constant = (obj?.showLefticon ?? false) ? 20:0
        if obj?.cellType == Constants.LOCATION{
            self.textLbl.textAlignment = .left
        }else{
            self.textLbl.textAlignment = .right
        }
    }
}
struct ButtonCellModel {
    var headingText: String?
    var titleText: String?
    var resultText : String?
    var showLefticon: Bool
    var cellType: String?
    
}


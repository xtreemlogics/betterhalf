//
//  CategoriesCell.swift
//  BetterHalf
//
//  Created by Usman on 26/01/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

class CategoriesCell: UITableViewCell {
    //MARK: - IBOUTLETS
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    var model : FilterModel?
    var cellHeight = 60
    var cellType: CellType?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func setupCollection(){
        collectionView.register(UINib(nibName: RoundButtonCell.className, bundle: nil), forCellWithReuseIdentifier: RoundButtonCell.className)
        // IF ODD THEN ADD ONE ELSE /2
        var dataArr = [CategoryButton]()
        if cellType == .Category{
            dataArr = self.model?.categoryArr ?? [CategoryButton]()
        }else if cellType == .Education{
            dataArr = self.model?.educationArr ?? [CategoryButton]()
        }else{
            dataArr = self.model?.marriageArr ?? [CategoryButton]()
        }
        collectionViewHeight.constant = CGFloat(dataArr.count % 2 == 0 ? (((dataArr.count / 2)) * (cellHeight + 15))  : (((dataArr.count/2) + 1) * cellHeight + 35))
        if cellType == CellType.Category{
            headingLbl.font = UIFont.systemFont(ofSize: 13)
            headingLbl.textColor =  UIColor(hexString: "#9B9B9B")
            
        }else{
            headingLbl.font = UIFont.boldSystemFont(ofSize: 17)
            headingLbl.textColor = UIColor(hexString: "#4A4A4A")
        }
        switch cellType {
        case .Category:
            headingLbl.text = Constants.WHAT_ARE_THE_SOME_IMPORTANT_CATEGORIES_FOR_CHOOSING_YOUR_BETTERHALF
        case .Education:
            headingLbl.text = Constants.EDUCATION
        case .Marriage:
            headingLbl.text = Constants.MARRIAGE_HISTORY
        default:
            print("no heading")
        }
        collectionView.reloadData()
    }
    
}
extension CategoriesCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: cellHeight)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if cellType == .Category{
            return self.model?.categoryArr?.count ?? 0
        }else if cellType == .Education{
            return self.model?.educationArr?.count ?? 0
        }else {
            return self.model?.marriageArr?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RoundButtonCell.className, for: indexPath) as! RoundButtonCell
        let model:CategoryButton?
        if cellType == .Category{
            model = self.model?.categoryArr?[indexPath.row]
        }else if cellType == .Education {
            model = self.model?.educationArr?[indexPath.row]
        }else{
            model = self.model?.marriageArr?[indexPath.row]
        }
        cell.model = self.model
        cell.configCell(model: model, cellType: self.cellType)
        cell.btnTapped = {
            if self.cellType == CellType.Education{
                self.model?.defaultEducation = model?.title ?? ""
//                self.collectionView.performBatchUpdates({
                    self.collectionView.reloadData()
//                }, completion: nil)
            }else if self.cellType == CellType.Marriage{
                self.model?.defaultMarriage = model?.title ?? ""
//                self.collectionView.performBatchUpdates({
                    self.collectionView.reloadData()
//                }, completion: nil)
            }else{
                model?.isSelected = !(model?.isSelected ?? true)
                self.collectionView.performBatchUpdates({
                    self.collectionView.reloadItems(at: [indexPath])
                }, completion: nil)
            }
            
        }
        return cell
    }
    
    
}

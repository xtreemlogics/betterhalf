//
//  QuestionCollectionCell.swift
//  BetterHalf
//
//  Created by Usman on 14/02/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

class QuestionCollectionCell: UICollectionViewCell {
    @IBOutlet weak var headerLbl:UILabel!
    @IBOutlet weak var footerHeaderLbl:UILabel!
    @IBOutlet weak var footerDetailLbl:UILabel!
    @IBOutlet weak var footerView:UIView!
    @IBOutlet weak var footerImg:UIImageView!
    @IBOutlet weak var bottomImg:UIImageView!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var imgLeadingConstraint:NSLayoutConstraint!
    var testModel:TestModel?
    var currentIndex = 0
    var answerTapped:((Int)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(){
        headerLbl.text = testModel?.title ?? ""
        footerHeaderLbl.text = testModel?.footerHeading ?? ""
        footerDetailLbl.text = testModel?.footerDetail ?? ""
        footerImg.image = testModel?.footerImg != "" ? UIImage(named: testModel?.footerImg ?? "") : nil
        if testModel?.bottomImg != ""{
            bottomImg.image = UIImage(named: testModel?.bottomImg ?? "")
        }else{
            bottomImg.isHidden = true
        }
        tableView.register(UINib(nibName: TestRoundButtonCell.className, bundle: nil), forCellReuseIdentifier: TestRoundButtonCell.className)
        tableView.reloadData()
        if footerHeaderLbl.text == "" && footerDetailLbl.text == "" {
            footerView.isHidden = true
        }else{
            footerView.isHidden = false
        }
        if testModel?.bottomImg ?? "" == "group87"{
            imgLeadingConstraint.constant = self.frame.width / 2
        }else{
            imgLeadingConstraint.isActive = true
        }
        
    }

}
extension QuestionCollectionCell:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testModel?.buttonArr.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TestRoundButtonCell.className, for: indexPath) as! TestRoundButtonCell
        cell.testModel = testModel
        cell.collectionViewIndex = self.currentIndex
        cell.configCell(title: testModel?.buttonArr[indexPath.row], btnWidth: testModel?.answerBtnWidth ?? ButtonSize.Small)
        cell.answerSelected = {(answer) in
            self.testModel?.selectedAnswer = answer
            self.tableView.reloadData()
            self.answerTapped?(self.currentIndex)
        }
        return cell
    }
    
    
}

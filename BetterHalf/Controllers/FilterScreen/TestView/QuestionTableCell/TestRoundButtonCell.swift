//
//  TestRoundButtonCell.swift
//  BetterHalf
//
//  Created by Usman on 14/02/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

protocol AnswerCellDelegate {
    func answerBtnTapped(sender:UIButton,indexOfCollection:Int)
}

class TestRoundButtonCell: UITableViewCell {
    //MARK: - IBOUTLETS
    @IBOutlet weak var titleBtn: UIButton!
    @IBOutlet weak var titleBtnWidth: NSLayoutConstraint!
    var answerSelected:((String?)->())?
    var testModel:TestModel?
    var collectionViewIndex = 0
    var delegate:AnswerCellDelegate?
    public lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds
        l.colors = [UIColor(hexString: "#ff62a5").cgColor, UIColor(hexString: "#ff8960").cgColor]
        l.startPoint = CGPoint(x: 0.0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.cornerRadius = 25
        layer.addSublayer(l)
        return l
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(title:String?,btnWidth: ButtonSize){
        if testModel?.selectedAnswer == title{
            titleBtn.layer.insertSublayer(gradientLayer, at: 0)
            titleBtn.setTitleColor(UIColor.white, for: .normal)
        }else{
            gradientLayer.removeFromSuperlayer()
            titleBtn.setTitleColor(UIColor(hexString: "#4A4A4A"), for: .normal)
        }
        switch btnWidth {
        case .Small:
            titleBtnWidth.constant = self.contentView.frame.width * 0.4
        case .Medium:
            titleBtnWidth.constant = self.contentView.frame.width * 0.55
        case .Large:
            titleBtnWidth.constant = self.contentView.frame.width * 0.75
        default:
            print("nothing")
        }
        titleBtn.setTitle(title ?? "", for: .normal)
    }
    
    @IBAction func answerButtonTapped(_ sender: UIButton){
        answerSelected?(sender.titleLabel?.text)
        delegate?.answerBtnTapped(sender: sender, indexOfCollection: collectionViewIndex)
    }
}

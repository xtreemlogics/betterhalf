//
//  TestMainView.swift
//  BetterHalf
//
//  Created by Usman on 13/02/2020.
//  Copyright © 2020 Mudassir. All rights reserved.
//

import UIKit

class TestMainView: UIView {
    //MARK:- IBOUTLETS
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var progressView:UIProgressView!
    var testArr = [TestModel]()
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        let view = instanceFromNib()
        view.frame = bounds
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIView.AutoresizingMask.flexibleWidth,
            UIView.AutoresizingMask.flexibleHeight
        ]
        collectionView.register(UINib(nibName: QuestionCollectionCell.className, bundle: nil), forCellWithReuseIdentifier: QuestionCollectionCell.className)
        collectionView.delegate = self
        collectionView.dataSource = self
        addSubview(view)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.mainView.roundCorners(corners: [.topLeft,.topRight], radius: CGFloat(16))
        let progress = (Float((1))/Float(testArr.count))
        self.progressView.setProgress(Float(progress), animated: true)
    }
    
    private func instanceFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton){
        for obj in testArr{
            obj.selectedAnswer = ""
        }
        self.removeFromSuperview()
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton){
        guard let visibleCell = self.collectionView.visibleCells.first else{return}
        if let indexPath = self.collectionView.indexPath(for: visibleCell){
            movePrevious(index: indexPath.row)
        }
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
            layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
}

extension TestMainView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return testArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: QuestionCollectionCell.className, for: indexPath) as! QuestionCollectionCell
        cell.testModel = testArr[indexPath.row]
        cell.currentIndex = indexPath.row
        cell.answerTapped = { index in
            self.moveNext(index: index)
        }
        cell.configCell()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height

        return CGSize(width: width, height: height)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let visibleCell = self.collectionView.visibleCells.first else{return}
        if let indexPath = self.collectionView.indexPath(for: visibleCell){
            let progress = (Float((indexPath.row + 1))/Float(testArr.count))
            self.progressView.setProgress(Float(progress), animated: true)
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let visibleCell = self.collectionView.visibleCells.first else{return}
        if let indexPath = self.collectionView.indexPath(for: visibleCell){
            let progress = (Float((indexPath.row + 1))/Float(testArr.count))
            self.progressView.setProgress(Float(progress), animated: true)
        }
    }
    
    
    
}
extension TestMainView: AnswerCellDelegate{
    func answerBtnTapped(sender: UIButton, indexOfCollection: Int) {
        moveNext(index: indexOfCollection)
    }
    
    func moveNext(index:Int){
        if index < self.collectionView.numberOfItems(inSection: 0) && (index + 1 < self.collectionView.numberOfItems(inSection: 0)){
            self.collectionView.scrollToItem(at: IndexPath(row: index + 1, section: 0), at: .right, animated: true)
        }else if index == self.collectionView.numberOfItems(inSection: 0){
            self.removeFromSuperview()
        }
    }
    
    func movePrevious(index:Int){
        if index < self.collectionView.numberOfItems(inSection: 0) && (index - 1 < self.collectionView.numberOfItems(inSection: 0)){
            self.collectionView.scrollToItem(at: IndexPath(row: index - 1, section: 0), at: .right, animated: true)
        }
    }
    
}




import UIKit
import Alamofire

class AuthenticationAPIManager: APIManagerBase {
    
    
    func post(parameters:Dictionary<String, String>,
                type:HTTPMethod,
                query:String,
                success:@escaping DefaultAPISuccessClosure,
                failure:@escaping DefaultAPIFailureClosure
                ){
        self.serverRequestWith(route: query.createUrl(), params: parameters, requestType: type, encoding: JSONEncoding.default, success: success, failure: failure)
        
    }
    
    func get(parameters:Dictionary<String, String>,
                type:HTTPMethod,
                query:String,
                success:@escaping DefaultAPISuccessClosure,
                failure:@escaping DefaultAPIFailureClosure
                ){
        self.serverRequestWith(route: query.createUrl(), params: parameters, requestType: type, encoding: URLEncoding(destination:.queryString), success: success, failure: failure)
        
    }
    
}

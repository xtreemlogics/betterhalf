//
//  broken-rule.swift
//  CBCBank
//
//  Created by Rockville on 15/01/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import Foundation
struct BrokenRule {
    
    var propertyName :String
    var message :String
}

protocol ViewModel {
    
    var brokenRules :[BrokenRule] { get set}
    var isValid :Bool { mutating get }
}

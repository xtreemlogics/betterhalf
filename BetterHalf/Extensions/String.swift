//
//  UIView-Extension.swift
//  NoBolo
//
//  Created by Usman on 4/25/19.
//  Copyright © 2019 Usman. All rights reserved.
//

import Foundation
import Alamofire
extension String {
    
    var length: Int {
        return self.count
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func fromBase64() -> String {
        let data = Data(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
        return String(data: data!, encoding: String.Encoding.utf8)!
    }
    
    
    func toBase64() -> String {
        let data = self.data(using: String.Encoding.utf8)
        return data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    }
    
    func createUrl() -> URL{
        let urlString  = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        if let url = URL(string: urlString){
            return url
        }
        return  URL(string: "urlString")!
    }
    
    func isValidEmail() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        
        let result =  emailPredicate.evaluate(with: self)
        
        return result
        
    }
    
    func isValidPhoneNum() -> Bool {
        let PHONE_REGEX = "^[0-9]{6,15}$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result
    }
    
    func isValidName() -> Bool{
        let NAME_REGEX = "(?<! )[-a-zA-Z' ]{2,26}"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", NAME_REGEX)
        let result =  nameTest.evaluate(with: self)
        return result
        
    }
    mutating func replaceFirstOccurrence(original: String, with newString: String) {
        if let range = self.range(of: original) {
            replaceSubrange(range, with: newString)
        }
    }
    
    
    func isValidPassword() -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789")
        
        if rangeOfCharacter(from: characterset.inverted) != nil && length >= 8 {
            return true
            
        } else {
            return false
        }
        
    }
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    func toInt() -> Int? {
        return NumberFormatter().number(from: self)?.intValue
    }
    
    func khrCurrencyInputFormatting() -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
//        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 0
        formatter.minimumFractionDigits = 0
        
        let double = (self as NSString).doubleValue
        var str = formatter.string(from: NSNumber(value: double))!
        str = str.replacingOccurrences(of: ",", with: "")
        return str
    }
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = ""
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        var str = formatter.string(from: number)!
        str = str.replacingOccurrences(of: ",", with: "")
        return str
    }
    
    
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}



extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
    
    
}





import Foundation
class FilterModel {
    var categoryArr: [CategoryButton]?
    var educationArr: [CategoryButton]?
    var marriageArr: [CategoryButton]?
    var distanceSlider:SliderModel?
    var ageSlider: SliderModel?
    var weightSlider: SliderModel?
    var heightSlider:SliderModel?
    var defaultEducation = ""
    var defaultMarriage = ""
    
    init(catArr: [CategoryButton]?,eduArr: [CategoryButton]?,marrArr: [CategoryButton]?,distance: SliderModel?,age: SliderModel?,weight: SliderModel?,heigt: SliderModel?) {
        self.categoryArr = catArr
        self.educationArr = eduArr
        self.marriageArr = marrArr
        self.distanceSlider = distance
        self.ageSlider = age
        self.weightSlider = weight
        self.heightSlider = heigt
    }
}
class CategoryButton {
    var title: String?
    var isSelected: Bool?
    init(title:String?,isSelected:Bool?) {
        self.title = title
        self.isSelected = isSelected
    }
}
public enum SliderType{
    case SingleThumb,MultiThumb
}
class SliderModel {
    var heading:String?
    var value:String?
    var sliderType: SliderType?
    var minValue: Float?
    var maxValue: Float?
    var selectedValue: Float?
    var selectedMaxValue:Float?
    init(heading:String?,value:String?,type:SliderType?,min: Float?,max: Float?,selected: Float?,selectedMax:Float?) {
        self.sliderType = type
        self.minValue = min
        self.maxValue = max
        self.selectedValue = selected
        self.heading = heading
        self.value = value
        self.selectedMaxValue = selectedMax
    }
}

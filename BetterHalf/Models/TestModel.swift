//
//  TestModel.swift
//  BetterHalf
//
//  Created by Usman on 14/02/2020.
//  Copyright © 2020 Usman. All rights reserved.
//

import UIKit

class TestModel: NSObject {
    var title:String?
    var footerHeading:String?
    var footerDetail:String?
    var footerImg:String?
    var buttonArr = [String]()
    var selectedAnswer:String?
    var answerBtnWidth = ButtonSize.Small
    var bottomImg:String?
    
     init(dictionary:Dictionary<String,Any>) {
        title = dictionary["title"] as? String
        footerHeading = dictionary["footerHeading"] as? String
        footerDetail = dictionary["footerDetail"] as? String
        footerImg = dictionary["footerImage"] as? String
        bottomImg = dictionary["bottomImage"] as? String
        for obj in dictionary["buttonArr"] as? [String] ?? []{
            buttonArr.append(obj)
        }
        switch dictionary["buttonSize"] as? String {
        case "small":
            answerBtnWidth = .Small
        case "medium":
            answerBtnWidth = .Medium
        case "large":
            answerBtnWidth = .Large
        default:
            answerBtnWidth = .Small
        }
    }
}
public enum ButtonSize{
    case Small,Medium,Large
}
